# Are Leisure Activity and Health Interconnected after Retirement: Educational Differences

This was joined work with Oliver Huxhold. You find the article [here](http://www.sciencedirect.com/science/article/pii/S1040260816300211). It was published in *Advances in Live Course Research* in 2016.

For an overview of this project [see here](https://b-dx.gitlab.io/post3.html).

### Structure

Data preparation is conducted using stata 11. The _00_m_*.do_-File includes the central data preparation tasks.
Data analysis is conducted using MPlus. The file _Wetzel_Huxhold_BivariateDCSM.inp_ executes the the most parsimonious and therefore final bivariate dual-change-score-model.

## If you have questions,

please contact me [here](https://b-dx.gitlab.io/contact.html).
