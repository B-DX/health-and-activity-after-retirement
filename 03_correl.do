clear
set more off
capture log close
clear matrix
clear mata
set maxvar 9000

adopath + "H:\Projekte\stata_ado" 
adopath + "G:\Apps\Stata10\ado"

global mydate: di %tdCYND date(c(current_date), "DMY")

*net search runmplus

global path_in	   		"L:\DEAS\00_wellenuebergreifend\03_Daten\01_Panel"
global path_temp 		"H:\Diss\01_DA\05_Ruhestand_DEAS\temp"
global path_log  		"H:\Diss\01_DA\05_Ruhestand_DEAS\log"
global path_out			"H:\Diss\01_DA\05_Ruhestand_DEAS\dta"
global path_do			"H:\Diss\01_DA\05_Ruhestand_DEAS\do"
global path_tex			"H:\Diss\01_DA\05_Ruhestand_DEAS\tex"
global path_m			"G:\MAs\Wetzel\Ruhestand_DEAS\Data\mplus"
global path_g			"G:\MAs\Wetzel\Ruhestand_DEAS\graph"

cap mkdir $path_temp
cap mkdir $path_log
cap mkdir $path_out
cap mkdir $path_tex
cap mkdir $path_m
cap mkdir $path_g

log using "$path_log\ruhestand_ds$mydate.smcl", replace
di "$path_do -   $mydate    "

**********************************************************************************************************************
** DATUM LETZTE BEARBEITUNG DO-FILE: 26.01.12 - $mydate                                                        	        **
** ERSTER  BEARBEITER:               MWe                                                    			            ** 
** LETZTER BEARBEITER:               MWe                                       				                        **
**********************************************************************************************************************

**********************************************************************************************************************
** DO-FILE: 								                        	    	   				                    **
** BESCHREIBUNG (maximal ausf�hrlich: Was soll wie gemacht werden...			   									**		
**********************************************************************************************************************
/* 
Automatisierte Analysen von �bergangsdaten in den Ruhestand

Gibt einfach eine Abh�ngige Variable in die local vars Liste ein und er berechnet die H�ufigkeiten,
die Mittelwerte (gesch�tzt und absolut), die Varianzen (gesch�tzt und absolut) und
perspektivisch auch die Verl�ufe nach Bildungsgruppen

*/
**********************************************************************************************************************
** 0: VORARBEITEN	 				                                                              				**
**********************************************************************************************************************
use "$path_temp\ruhestand_ds_2.dta", replace
set more off
set scheme lean1
global gropts ///
	plot1opts(lcolor(gs9) mcolor(gs9)) ci1opts(lcolor(gs9))						///
	legend(row(1) pos(6)) ylabel(44(2)56) 
global gropts2 ///
	plot1opts(lcolor(gs9) mcolor(gs9)) ci1opts(lcolor(gs9))						///
	plot2opts(lcolor(black) mcolor(black)) ci1opts(lcolor(black))						///
	legend(row(1) pos(6))
	
drop if !inrange(d_rs,0,10)	
	
gen d_rs2 = d_rs*d_rs
replace pc501 = -pc501 + 6
replace bil3 = bil3 - 2
replace bil3 = 0 if bil3 == -1
rename nwgroesse nwgr
lab var d_rs "Jahre im Ruhestand"

xtset fallnum d_rs

global levels 0 1 2 3 4 5 6 7 8 9 10
local vars anzphy sf36 pc501 aktint13 aktanzg13 hheink nwgr
foreach var of varlist `vars' {

	global tstd = 1
	if $tstd == 1 {
		sum `var' if d_rs == 0
		local sd `r(sd)'
		local mean `r(mean)'
		for any $levels: replace `var' = ((`var' - `mean') / `sd' ) * 10 + 50 if d_rs == X
		tabstat `var', s(mean v n) by(d_rs)
}
}

****************
** Allgemeine Verl�ufe - ohne Bildungsgruppen
****************

xtreg sf36 (c.d_rs##c.d_rs) bil3 sex a_rs , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix msf36a = r(b)'
marginsplot, xdim(d_rs) $gropts name(gsf36a, replace) ti(sf36) nodraw

* 
xtreg pc501 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mpc501a = r(b)'
marginsplot, xdim(d_rs) $gropts name(gpc501a, replace) ti(pc501) nodraw

*
xtreg aktanzg13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktanza = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktanza, replace) ti(aktanz) nodraw

*
xtreg aktint13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktinta = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktinta, replace) ti(aktint) nodraw

*
xtreg nwgr (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mnwgra = r(b)'
marginsplot, xdim(d_rs) $gropts name(gnwgra, replace) ti(nwgr) nodraw

*
xtreg hheink (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mhheinka = r(b)'
marginsplot, xdim(d_rs) $gropts name(ghheinka, replace) ti(hheink) nodraw

graph combine gsf36a gpc501a gaktanza gaktinta gnwgra ghheinka, ycomm col(2) ///
			imargin(0 0 ) title("Verl�ufe allgemein") ///
			note("DEAS1996-2011, kontr. f�r sex, a_rs") ///
			ysize(8) xsize(5.5) 
graph export "$path_g\gr1_allg.png", as(png) replace

for any sf36 pc501 aktanz aktint nwgr hheink: svmat mXa

pwcorr msf36a1 mpc501a1 maktanza1 maktinta1 mnwgra1 mhheinka1



****************
** bildungsspezifische Verl�ufe - ohne Bildungsgruppen
****************

****************
** Mittel und Niedriggebildet
****************
preserve
keep if bil3 == 0

xtreg sf36 (c.d_rs##c.d_rs) bil3 sex a_rs , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix msf36b = r(b)'
marginsplot, xdim(d_rs) $gropts name(gsf36b, replace) ti(sf36) nodraw

* 
xtreg pc501 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mpc501b = r(b)'
marginsplot, xdim(d_rs) $gropts name(gpc501b, replace) ti(pc501) nodraw

*
xtreg aktanzg13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktanzb = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktanzb, replace) ti(aktanz) nodraw

*
xtreg aktint13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktintb = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktintb, replace) ti(aktint) nodraw

*
xtreg nwgr (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mnwgrb = r(b)'
marginsplot, xdim(d_rs) $gropts name(gnwgrb, replace) ti(nwgr) nodraw

*
xtreg hheink (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mhheinkb = r(b)'
marginsplot, xdim(d_rs) $gropts name(ghheinkb, replace) ti(hheink) nodraw

restore

graph combine gsf36b gpc501b gaktanzb gaktintb gnwgrb ghheinkb, ycomm col(2) ///
			imargin(0 0 ) title("Verl�ufe Niedrig- und Mittelgebildete") ///
			note("DEAS1996-2011, kontr. f�r sex, a_rs") ///
			ysize(8) xsize(5.5) 
graph export "$path_g\gr2_low_mid.png", as(png) replace

for any sf36 pc501 aktanz aktint nwgr hheink: svmat mXb

pwcorr msf36b1 mpc501b1 maktanzb1 maktintb1 mnwgrb1 mhheinkb1



****************
** Hochgebildet
****************
preserve
keep if bil3 == 1

xtreg sf36 (c.d_rs##c.d_rs) bil3 sex a_rs , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix msf36c = r(b)'
marginsplot, xdim(d_rs) $gropts name(gsf36c, replace) ti(sf36) nodraw

* 
xtreg pc501 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mpc501c = r(b)'
marginsplot, xdim(d_rs) $gropts name(gpc501c, replace) ti(pc501) nodraw

*
xtreg aktanzg13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktanzc = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktanzc, replace) ti(aktanz) nodraw

*
xtreg aktint13 (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix maktintc = r(b)'
marginsplot, xdim(d_rs) $gropts name(gaktintc, replace) ti(aktint) nodraw

*
xtreg nwgr (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mnwgrc = r(b)'
marginsplot, xdim(d_rs) $gropts name(gnwgrc, replace) ti(nwgr) nodraw

*
xtreg hheink (c.d_rs##c.d_rs) bil3 sex a_rs  , mle
* estat recovariance, corr
margins, at(d_rs=(0(1)10))  
matrix mhheinkc = r(b)'
marginsplot, xdim(d_rs) $gropts name(ghheinkc, replace) ti(hheink) nodraw

restore

graph combine gsf36c gpc501c gaktanzc gaktintc gnwgrc ghheinkc, ycomm col(2) ///
			imargin(0 0 ) title("Verl�ufe f�r Hochgebildete") ///
			note("DEAS1996-2011, kontr. f�r sex, a_rs") ///
			ysize(8) xsize(5.5) 
graph export "$path_g\gr2_high.png", as(png) replace

for any sf36 pc501 aktanz aktint nwgr hheink: svmat mXc

pwcorr msf36c1 mpc501c1 maktanzc1 maktintc1 mnwgrc1 mhheinkc1


***
** Corr-Zusammenfassung
***
pwcorr msf36a1 mpc501a1 maktanza1 maktinta1 mnwgra1 mhheinka1
pwcorr msf36b1 mpc501b1 maktanzb1 maktintb1 mnwgrb1 mhheinkb1
pwcorr msf36c1 mpc501c1 maktanzc1 maktintc1 mnwgrc1 mhheinkc1


exit

**********************************************************************************************************************
** : ENDE                                                                                                           **
**********************************************************************************************************************

log close
clear
exit
