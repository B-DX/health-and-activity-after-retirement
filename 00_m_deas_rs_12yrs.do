clear
set more off
capture log close
clear matrix
clear mata
set maxvar 9000

adopath + "H:\Projekte\stata_ado" 
adopath + "G:\Apps\Stata10\ado"

global mydate: di %tdCYND date(c(current_date), "DMY")

*net search runmplus

global path_in	   		"L:\DEAS\00_wellenuebergreifend\03_Daten\01_Panel"
global path_temp 		"C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\temp"
global path_log  		"C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\log"
global path_out			"C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\dta"
global path_do			"C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\do"
global path_tex			"C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\tex"
global path_m			"G:\MAs\Wetzel\Ruhestand_DEAS\Data\mplus"
global path_g			"G:\MAs\Wetzel\Ruhestand\graph"

cap mkdir $path_temp
cap mkdir $path_log
cap mkdir $path_out
cap mkdir $path_tex
cap mkdir $path_m

log using "$path_log\ruhestand_ds$mydate.smcl", replace
di "$path_do -   $mydate    "
**********************************************************************************************************************
** DATUM LETZTE BEARBEITUNG DO-FILE: 02-07-12                                              	        **
** ERSTER  BEARBEITER:               MWe                                                    			            ** 
** LETZTER BEARBEITER:               MWe                                       				                        **
**********************************************************************************************************************

**********************************************************************************************************************
** DO-FILE: 								                        	    	   				                    **
** BESCHREIBUNG (maximal ausf�hrlich: Was soll wie gemacht werden...			   									**		
**********************************************************************************************************************
/* 
Erzeugung eines Datensatzes, mit dem �berg�nge in den Ruhestand untersucht werden k�nnen.
Um es f�r MPlus zu erzeugen, wird ein wide-Datensatz gebaut, der beim Ruhestands�bergang seinen Zeitpunkt "0" hat

Variablenliste siehe Modell_Variablen.xlsx

pc32 		Jahr erste hauptberufliche Erwerbst�tigkeit
pc113		Jahr, bis zu dem ZP hauptberuflich erwerbst�tig war
pc175		Bis zu welchem Jahr waren Sie hauptberuflich erwerbst�tig
		subjektive Arbeitsbelastung ?!?



*/
**********************************************************************************************************************
** 0: VORARBEITEN	 				                                                              				**
**********************************************************************************************************************
* 2 Eingangsvariablenlisten: keepvars aus Excel-Sheet: Modellvariablen 
*							 keepvars2 sind allgemeine, immer ben�tigte Variablen
global keepvars3 fallnum welle intjahr stich alter nat_ewo bildung3 isced int_do
		 
			
do "$path_do/01_keepvars.do"		// global mit keepvars


use $keepvars1  $keepvars2 $keepvars3 using "$path_in\DEAS_P96-11_l_v0.5.dta", clear
numlabel _all, add force det
mvdecode _all, mv(-1=.a \ -2=.b \ -3=.c \ -4=.d \ -5=.e \ -6=.f \ -7=.g \ -8=.h \ -9=.i)

* tabstat _all, s(n) by(welle)

* mit Ver�nderungen gekennzeichnete Variablen - Xpc
tab Xpc100 welle, col	// ok
rename Xpc100 pc100
tab Xpc32a welle, col	// ok
rename Xpc32a pc32a
tab pc106 welle, col	// ok
oneway Xpc118 welle, mean //  ok
rename Xpc118 pc118
oneway lz welle, mean // ok
tab lz welle


* Umbenennungen

rename isced bil3
rename pc1 sex

saveold "$path_temp\ruhestand_ds_1.dta", replace
d, s


****************************************************************************************
***! VARIABLENGENERIERUNG
****************************************************************************************
* do "$path_do/gen_ruhestzp_3.do"		// 


do "$path_do/gen_vars.do"		// 



****************************************************************************************
***! Graphiken
****************************************************************************************

* Erzeugung einer Abbildung mit der Anzahl von Beobachtungen pro Bin 
local an = 0 		// Schalter: 0 ist aus, 1 ist an
if `an' == 1 {
histogram d_rs, frequ width(1) name(histo1, replace) nodraw
}

replace d_rs = 13 if d_rs > 12 & !mi(d_rs)
replace d_rs = -8 if d_rs < 0

bys job:  tabstat a_rs, s(n) by(d_rs)
bys bil3: tabstat a_rs, s(n) by(d_rs)

if `an' == 1 {
histogram d_rs, frequ width(1) name(histo2, replace) nodraw
graph combine histo1 histo2
graph export 	"$path_g\g_histo_d_rs.png", replace


local graphlist pdread3 isei88zp pa na lone6 nwgroesse hope hheink aee_oecd ///
			    bildung3 bildung4 anzkind anzenk anzphy sf36
foreach var of local graphlist {
	graph box `var', over(d_rs) xsize(8) ysize(4)
	graph export "$path_g\box_`var'.png", replace
	}
}



//*************************
// Personen im AltersRange
//*************************
* Alter zum Ruhestand konstant innerhalb einer Person?
count if fallnum == fallnum[_n+1] & a_rs != a_rs[_n+1]
bys fallnum: replace a_rs = a_rs[_n-1] if !mi(a_rs[_n-1])


*
* Hier M�ssen wir mal gemeinsam �berlegen!!!
*
* 
sort fallnum welle
count if fallnum != fallnum[_n+1]						//   14713 Personen
count													//   23083 Beobachtungen

drop if stich == 0
count if fallnum != fallnum[_n+1]						//   14127 Personen
count													//   22497 Beobachtungen

drop if d_rs == .										//   10488 Beobachtungen nicht im RS (47,6 %)
count if fallnum != fallnum[_n+1]						//    7133 Personen
count													//   12009 Beobachtungen

sort fallnum welle
count													//   12009 Beobachtungen
count if fallnum != fallnum[_n+1] & inlist(d_rs,13,-8)
drop if inlist(d_rs,13,-8)								//    5522 Beobachtungen nicht im Zeitraum (40,7 %)
count if fallnum != fallnum[_n+1]						//    4313 Personen
count													//    6487 Beobachtungen

keep  if inrange(a_rs,57,65)							//    459 Beobachtungen nicht im Altersrange (40,7 %)
count if fallnum != fallnum[_n+1]						//    3990 Personen
count													//    6028 Beobachtungen

bys fallnum: egen flag = total(Xpc102==1)				//
drop if flag != 0										//     773 Beobachtungen mit EWT im Ruhestand 
sort fallnum welle
count if fallnum != fallnum[_n+1]						//    3547 Personen
count													//    5255 Beobachtungen
drop flag

* weitere Selektion siehe weiter unten



bys fallnum: gen N = _N
mvdecode N, mv(1=.a)
tab N if fallnum != fallnum[_n+1]						//    2382 Personen nur eine Beobachtung
														// 	  1649 Personen bleiben
tab N													//    3686 Beobachtungen bleiben


tabstat sex, s(n mean) by(d_rs)



* do "$path_do/des_bil3.do"		// 
* drop pc425_* pc426* pc427* pc428* pc429* pc430* pc431* ///
*		pc432* pc433*  



saveold "$path_temp\ruhestand_ds_2.dta", replace

/*
****************************************************************************************
***! Listen
****************************************************************************************
Aus der Ausgangsliste des Excel-Files entstehen 3 Listen:
	a) keepvars: Liste von Variablen, die aus dem Datensatz geladen werden sollen
		z.B. read* (alle 12 read-Variablen)
	b) stumpf: Liste aus den geladenen Variablen, die als Variablenstumpf zum Umklappen
	           im reshape-Prozess ben�tigt werden
		z.B. read1 read2 read3 read4 ... read12
	c) order: Liste anhand der Datensatz in der Bin-(wide)-Struktur geordnet werden soll
		z.B. read1_7 .. read10 ... read130 oder read2_7 ... read20 ... read230 
			(Variable read1 von Zeitpunkt -1 �ber 0 zu ZP 30)
			*/
use "$path_temp\ruhestand_ds_2.dta", clear			
			
		
// Variablen aus Liste l�schen
local a fallnum	welle d_rs $keepvars1 bil3 a_rs rsj_kons stich sex job westost ///
							ehr1 ehr2 ehramt lastjob sf36_k pc501_k ///
							pc425_1 pc426	pc425_7a pc425_5a pc425_6 ///
							pc425_2 pc427 pc431 pc425_3a pc429 pc428 pc430 ///
							pc432 pc425_4 pdhealt1   pdhealt11  pdhealt21 ///
							pdhealt7   pdhealt17  pdhealt3   ///
							pdhealt13  pdhealt23 pdhealt9   pdhealt19  pdhealt39 ///
							pdhealt5   pdhealt15  
							
							
// Variablen zur Liste hinzuf�gen
qui ds _all
local b `r(varlist)' 
// Variablen hinzuf�gen
local c fallnum welle d_rs bil3 a_rs rsj_kons stich sex job lastjob westost ehramt sf36_k pc501_k ///
							pc425_1 pc426	pc425_7a pc425_5a pc425_6 ///
							pc425_2 pc427 pc431 pc425_3a pc429 pc428 pc430 ///
							pc432 pc425_4 pdhealt1   pdhealt11  pdhealt21 ///
							pdhealt7   pdhealt17  pdhealt3   ///
							pdhealt13  pdhealt23 pdhealt9   pdhealt19  pdhealt39 ///
							pdhealt5   pdhealt15  
* di "`c'"

local help: list b - a 
di "`help'"

global keepvars4 `c' `help' 
di "$keepvars4"
di "`a'"
di "`help'"
global help `help'


* Variablenauswahl
keep $keepvars4
ds

// der folgende Abbruch scheint ein bug in stata zu sein:
rename * *_
rename (fallnum_ ) (fallnum )
/*	3 Jahres Bins
gen d_rs = .
* replace d_rs = floor(d_rs_/3)
replace d_rs = 0 if inrange(d_rs_,-1,1)
replace d_rs = -1 if inrange(d_rs_,-4,-2)
replace d_rs = -2 if inrange(d_rs_,-7,-5)
replace d_rs = 1 if inrange(d_rs_,2,4)
replace d_rs = 2 if inrange(d_rs_,5,7)
replace d_rs = 3 if inrange(d_rs_,8,10)
replace d_rs = 4 if inrange(d_rs_,9,11)

tab2 d_rs_ d_rs , m
drop d_rs_			*/

*	1 Jahres Bins
rename d_rs_ d_rs



// Sortierung und Variablenst�mpfe
global levels  n6 n5 n4 n3 n2 n1 0 1 2 3 4 5 6 7 8 9 10 11 12
global order fallnum
global stumpf 

foreach var of global help {
	foreach suf of global levels {
		global order $order `var'_`suf'
		}
	global stumpf $stumpf `var'_
	}
*di "$keepvars"
*di "$keepvars2"
*di "$order"
*global stumpf $help
di "$stumpf"


****************************************************************************************
***! BINS-KONTROLLE
****************************************************************************************

* Um die Bins zu bilden, m�ssen die Distanzen zw. RS und Interviewzeitpunkt
* einmalig sein (sonst w�rde es zu einer �berschreibung einzelner Zellen kommen)
bys fallnum: gen flag = d_rs == d_rs[_n+1]
bys fallnum: replace flag = 1 if flag[_n-1] == 1
replace flag = 0 if d_rs == .
tab flag											

drop flag


* negative Bins werden statt mit - (minus) mit n (negativ) bezeichnet
*	Grund: Variablennamen mit "-" in stata verboten
sort fallnum d_rs 
tostring d_rs, replace
foreach num of numlist 12 / 1 {
	replace d_rs = "n`num'" if d_rs == "-`num'"  
	}
tab d_rs
drop if d_rs == "."

* Distanz zum Ruhestand konstant innerhalb einer Person?
sort fallnum d_rs
gen flag1 = d_rs == d_rs[_n+1] & fallnum == fallnum[_n+1]
tab flag1
bys fallnum: egen flag2 = total(flag1)			
list fallnum welle intjahr d_rs a_rs rsj if flag2 == 1
* konstant --> okay
drop flag*

* Bil3 zum Ruhestand konstant innerhalb einer Person?
count if fallnum == fallnum[_n+1] & bil3 != bil3[_n+1] 
gen flag = fallnum == fallnum[_n+1] & bil3 != bil3[_n+1] 
sort fallnum 
replace bil3 = bil3[_n-1] if !mi(bil3[_n-1]) & fallnum == fallnum[_n-1]
replace bil3 = bil3[_n+1] if !mi(bil3[_n+1]) & fallnum == fallnum[_n+1]
replace bil3 = bil3[_n+1] if !mi(bil3[_n+1]) & fallnum == fallnum[_n+1]
* konstant --> okay
drop flag* 

* Westost konstant innerhalb einer Person?
sort fallnum westost
count if fallnum == fallnum[_n+1] & westost != westost[_n+1]
* 4 nicht konstant 
bys fallnum (welle): replace westost = westost[_n-1] if !mi(westost[_n-1])
bys fallnum (welle): replace westost = westost[_n+1] if !mi(westost[_n+1])

* job / lastjob - wo kommen denn die sysmis her?
tab job welle, m


**********************
*	Weitere Selektion
**********************
bys fallnum: egen flag = total(lastjob==4)				//
drop if flag != 0										//     485 Beobachtungen aus Vorruhestand 
sort fallnum welle
count if fallnum != fallnum[_n+1]						//    3187 Personen
count													//    4769 Beobachtungen
drop flag 

bys fallnum: egen flag = total(mi(lastjob))				//
bys fallnum: egen flag2 = total(mi(bil3))
drop if flag != 0 | flag2 != 0							//     545 Beobachtungen aus Vorruhestand 
sort fallnum welle
count if fallnum != fallnum[_n+1]						//    2873 Personen
count													//    4224 Beobachtungen
drop flag flag2

bys fallnum: egen flag = total(int_do == 2)
drop if flag == 0 										//     746 Personen, die nie DO ausf�llten 
sort fallnum welle
count if fallnum != fallnum[_n+1]						//    2873 Personen
count													//    4224 Beobachtungen
drop flag 


****************************************************************************************
***! Statistik
****************************************************************************************
tab bil3 d_rs, m col
bys job: tab bil3 d_rs, m col
encode d_rs, gen(welle)
tsset fallnum welle
xtdes, pattern(30)

bys job: tab bil3 d_rs, m col

recode rsj (1984/1999=0)(2000/2011=1), gen(coh)
destring d_rs, g(w2)

sort falln w2
gen cons = 1
xtset fallnum w2
xtreg aktint13 coh w2, re	
runmlwin aktint13 cons c.w2##coh, level2(fallnum: cons w2 coh) level1(w2: cons) batch
xtreg aktint13 cons c.w2##coh, re

runmlwin anzphy cons c.w2##coh, level2(fallnum: cons w2 coh) level1(w2: cons) batch
xtreg anzphy cons c.w2##coh, re

saveold "$path_temp\deskript.dta", replace

drop pc425_* pc426* pc427* pc428* pc429* pc430* pc431* ///
		pc432* pc433*  coh cons w2
drop pdhealt*

drop welle
****************************************************************************************
***! RESHAPE und SORTIEREN
****************************************************************************************

format %10.4f lone6_ pa_ na_ hope_

reshape wide $stumpf, i(fallnum) j(d_rs) string

*ds 
order $order

egen N = rownonmiss(alter*)
tab N

tabstat aktint* if N != 1, by(bil3) s(N)

rename *_ *

merge 1:m fallnum using "C:\Wetzel\Diss\01_DA\05_Ruhestand_DEAS\do\bildung\bil3n2.dta"
keep if _m == 3
drop _m
tab bil3 bil3n, cell

saveold "$path_temp\ruhestand_ds_3.dta", replace

tab sex, m						// 4701 Personen, davon 
count if N > 1					// nur 1915 die mehrmals befragt wurden

tabstat intjahr_n2 - intjahr_4, s(mean v n) by(bil3)


egen count = rownonmiss(intjahr*)
tab count

exit


* log close
clear
exit
