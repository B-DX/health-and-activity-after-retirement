*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*        GENERIERUNG VON VARIABLEN 
*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
use "$path_temp\ruhestand_ds_1.dta", clear

merge m:m fallnum  using "$path_temp\rsj_kons.dta"
tab _m

drop if _m == 2				// Platzhalter entfernen...
drop _m 
count if mi(rsj_kons)	

sort fallnum welle
count if !mi(rsj_kons)
count if !mi(rsj_kons) & fallnum != fallnum[_n+1]
		// W3: 6664 Personen -  9622  Zeitpunkte
		// W4: 7213 Personen - 12090 Zeitpunkte
		
* keep if rsj == rsj_kons
		
* Distanz zu Ruhestandszeitpunkt 
gen d_rs = intjahr - rsj_kons
lab var d_rs "Distanz Ruhestand zu Befragungszeitpunkt_neu" 
tab1 d_rs*, m

* unterschiedliche Altersangaben �ber die Wellen
bys fallnum (welle): gen flag = 1 if pc2_3 != pc2_3[_n+1] & fallnum == fallnum[_n+1]
bys fallnum: egen flag1 = total(flag)
tab flag1
* bro fallnum welle pc2_3 alter intjahr if flag1 != 0
drop flag flag1

drop if fallnum == 1002142 & welle == 4
drop if fallnum == 3000910 & welle == 4
replace	pc2_3	=	1914	if	welle	==	3	&	fallnum	==	1000187
replace	pc2_3	=	1912	if	welle	==	3	&	fallnum	==	1000264
replace	pc2_3	=	1914	if	welle	==	1	&	fallnum	==	1000467
replace	pc2_3	=	1953	if	welle	==	4	&	fallnum	==	1002152
replace	pc2_3	=	1942	if	welle	==	1	&	fallnum	==	1002644
replace	pc2_3	=	1948	if	welle	==	4	&	fallnum	==	1003435
replace	pc2_3	=	1949	if	welle	==	2	&	fallnum	==	1003744
replace	pc2_3	=	1946	if	welle	==	1	&	fallnum	==	1003825
replace	pc2_3	=	1918	if	welle	==	3	&	fallnum	==	1004637
replace	pc2_3	=	1947	if	welle	==	3	&	fallnum	==	2000233
replace	pc2_3	=	1919	if	welle	==	4	&	fallnum	==	2000305
replace	pc2_3	=	1946	if	welle	==	4	&	fallnum	==	2000947
replace	pc2_3	=	1958	if	welle	==	2	&	fallnum	==	2002162
replace	pc2_3	=	1942	if	welle	==	2	&	fallnum	==	2002754
replace	pc2_3	=	1942	if	welle	==	4	&	fallnum	==	3000089
replace	pc2_3	=	1934	if	welle	==	3	&	fallnum	==	3000482
replace	pc2_3	=	1986	if	welle	==	3	&	fallnum	==	3000665
replace	pc2_3	=	1962	if	welle	==	3	&	fallnum	==	3000850
replace	pc2_3	=	1956	if	welle	==	4	&	fallnum	==	3001123
replace	pc2_3	=	1963	if	welle	==	4	&	fallnum	==	3001208
replace	pc2_3	=	1952	if	welle	==	4	&	fallnum	==	3001561
replace	pc2_3	=	1950	if	welle	==	3	&	fallnum	==	3001707
replace	pc2_3	=	1949	if	welle	==	3	&	fallnum	==	3001738
replace	pc2_3	=	1932	if	welle	==	3	&	fallnum	==	3002015
replace	pc2_3	=	1954	if	welle	==	3	&	fallnum	==	3002372
replace	pc2_3	=	1943	if	welle	==	3	&	fallnum	==	3002568
replace	pc2_3	=	1952	if	welle	==	3	&	fallnum	==	3002869
replace	pc2_3	=	1955	if	welle	==	3	&	fallnum	==	3002946
replace	pc2_3	=	1933	if	welle	==	3	&	fallnum	==	3003297
replace	pc2_3	=	1954	if	welle	==	4	&	fallnum	==	3003482
replace	pc2_3	=	1933	if	welle	==	4	&	fallnum	==	3003497
replace	pc2_3	=	1933	if	welle	==	3	&	fallnum	==	3003627
replace	pc2_3	=	1931	if	welle	==	3	&	fallnum	==	3003628
replace	pc2_3	=	1963	if	welle	==	3	&	fallnum	==	3003857
replace	pc2_3	=	1923	if	welle	==	4	&	fallnum	==	3004002
replace	pc2_3	=	1946	if	welle	==	3	&	fallnum	==	3004149
replace	pc2_3	=	1967	if	welle	==	3	&	fallnum	==	3004378
replace	pc2_3	=	1954	if	welle	==	3	&	fallnum	==	3004571
replace	pc2_3	=	1942	if	welle	==	3	&	fallnum	==	3004650
replace	pc2_3	=	1937	if	welle	==	3	&	fallnum	==	3004674
replace	pc2_3	=	1939	if	welle	==	3	&	fallnum	==	3004775
replace	pc2_3	=	1934	if	welle	==	3	&	fallnum	==	3005186
replace	pc2_3	=	1941	if	welle	==	3	&	fallnum	==	3005439
replace	pc2_3	=	1959	if	welle	==	3	&	fallnum	==	3005543
replace	pc2_3	=	1929	if	welle	==	3	&	fallnum	==	3005681
replace	pc2_3	=	1957	if	welle	==	3	&	fallnum	==	3005993
replace	pc2_3	=	1957	if	welle	==	4	&	fallnum	==	3006063
replace	pc2_3	=	1924	if	welle	==	3	&	fallnum	==	3006077

replace alter = intjahr - pc2_3 if inlist(fallnum,1000187	, 1000264	, 1000467	, ///
	1002152	, 1002644	, 1003435	, 1003744	, 1003825	, 1004637	, 2000233	, ///
	2000305	, 2000947	, 2002162	, 2002754	, 3000089	, 3000482	, 3000665	, ///
	3000850	, 3001123	, 3001208	, 3001561	, 3001707	, 3001738	, 3002015	, ///
	3002372	, 3002568	, 3002869	, 3002946	, 3003297	, 3003482	, 3003497	, ///
	3003627	, 3003628	, 3003857	, 3004002	, 3004149	, 3004378	, 3004571	, ///
	3004650	, 3004674	, 3004775	, 3005186	, 3005439	, 3005543	, 3005681	, ///
	3005993	, 3006063	, 3006077	)



* Alter zum Ruhestandseintritt
gen a_rs = rsj_kons - (intjahr - alter)
lab var a_rs "Alter zum Renteneintritt"
sort fallnum welle
bys fallnum: gen flag = 1 if a_rs != a_rs[_n+1] & fallnum == fallnum[_n+1]
bys fallnum: egen flag2 = total(flag)

* sp�ter Einzelfallentscheidungen (nach Datenaufbereitung!!!)
* bro fallnum welle intjahr pc2_3 alter a_rs if flag2 == 1
tab flag2 
list fallnum a_rs rsj_kons intj alter if flag2 == 1
replace a_rs = a_rs[_n+1] if fallnum == fallnum[_n+1]
drop flag flag2

****************************************************************************************
*** Gewichtung
****************************************************************************************
ge pweig=.
replace pweig=weight_iq if weight_iq>0
bys fallnum (welle): gen n=_n
replace pweig=weight_id if n==1

ge dweig=.
replace dweig=weight_dq if weight_dq>0
bys fallnum (welle): gen n2=_n
replace dweig=weight_dd if n2==1
drop n n2

tabstat pwei dwei  , by(welle) s(n)
tab sex west [aw=pwei], cell
tab sex west [aw=dwei], cell


****************************************************************************************
***! Netzwerk
****************************************************************************************
* nwgroesse Netzwerkgroesse 
* nwgr_afam	au�erfamiliale Kontakte
* nw_famrel Verh�ltnis von fam zu au�erfam. Kontakten

tab2 pc600 welle, m
tab2 pc601_1 welle, m


tab pc607_8 welle, m /* korrekt? */

tab Xpc600 welle, m		// <-- BUG in NETZWERKVARIABLE WELLE 1 und 2

for any 1 2 3 4 5 6 7 8: gen afamX = inrange(pc601_X,500,905)
egen nwgr_afam = anycount(afam?), v(1)
tab2 nwgr_afam welle, m col
drop afam*

* Verh�ltnis von fam zu au�erfam Kontakte
gen nw_famrel = (nwgroesse - nwgr_afam) / nwgroesse * 100

drop pc600*  pc600* pc601*  Xpc602*  Xpc603* Xpc604* pc605* pc606* ///
		pc601*  pc602* pc603* pc604* pc607*

		
****************************************************************************************
***! Aktivit�ten
****************************************************************************************
* aktanzg 	Anzahl Aktivit�ten im Allgemeinen (t�glich, mehrmal und einmal die Woche)
* aktint		Sumscore der Intensit�t aller ausge�bten Aktivit�ten
* aktanzs1 	Anzahl sozialer Aktivit�ten (mit jemanden zusammen)
* aktanzs2	Anzahl sozialer Aktivit�ten (Art der Aktivit�t)
* aktdiv		Diversit�t von Aktiviten 
d pc425_* Xpc426 pc426* pc427* pc428* pc429* pc430* pc431* ///
		pc432* pc433*
* pc427 (pc433) - Sport machen (Personen treffen) f�llt raus, weil nicht in W1 (W1 & W2) erfragt		
		
tab2 Xpc426 welle, m
rename Xpc426 pc426

d pc425_1 pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a pc426 pc427 pc428 pc429 pc430 pc431 pc432
for any pc425_1 pc425_2 pc425_3a pc425_4 ///
	pc425_5a pc425_6 pc425_7a pc426 pc427 ///
	pc428 pc429 pc430 pc431 pc432: tab2 X welle, m

* aktanz 	Anzahl Aktivit�ten im Allgemeinen (t�glich, mehrmal und einmal die Woche)
egen aktanzg3 = anycount( pc425_1 pc425_2 pc425_4 pc425_6 pc426 pc427 pc428 ///
						pc429 pc430 pc431 pc432), v(1 2 3 4)
tab aktanzg3
oneway aktanzg3 d_rs if inrange(d_rs,-10,10), tab 
oneway aktanzg3 welle if inrange(d_rs,-10,10), tab 

egen aktanzg4 = anycount( pc425_1 pc425_2 pc425_4 pc425_6 pc426 pc427 pc428 ///
						pc429 pc430 pc431 pc432), v(1 2 3 4)
tab aktanzg4
oneway aktanzg4 d_rs if inrange(d_rs,-10,10), tab 
oneway aktanzg4 welle if inrange(d_rs,-10,10), tab 
						
egen aktanzg14 = anycount(pc425_1 pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a ///
						pc426 pc427 pc428 pc429 pc430 pc431 pc432), v(1 2 3 4)
tab aktanzg14

egen aktanzg13 = anycount(pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a ///
						pc426 pc427 pc428 pc429 pc430 pc431 pc432), v(1 2 3 4)
tab aktanzg13

oneway aktanzg4 d_rs if inrange(d_rs,-10,10), tab 
oneway aktanzg4 welle if inrange(d_rs,-10,10), tab 

						
						
* akint		Sumscore der Intensit�t aller ausge�bten Aktivit�ten
egen aktint_h = rowtotal(pc425_1 pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a ///
						pc426 pc427 pc428 pc429 pc430 pc431 pc432)
gen aktint = 60 - aktint_h
tab aktint
oneway aktint welle if inrange(d_rs,-10,10), tab 
drop aktint_h

egen aktint_h = rowtotal(pc425_1 pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a ///
						pc426 pc427 pc428 pc429 pc430 pc431 pc432)
gen aktint14 = 84 - aktint_h
tab aktint14
oneway aktint14 welle if inrange(d_rs,-10,10), tab 
drop aktint_h

egen aktint_h = rowtotal(pc425_2 pc425_3a pc425_4 pc425_5a pc425_6 pc425_7a ///
						pc426 pc427 pc428 pc429 pc430 pc431 pc432)
gen aktint13 = 78 - aktint_h
tab aktint13
oneway aktint13 welle if inrange(d_rs,-10,10), tab 
oneway aktint13 bil3 if inrange(d_rs,-10,10), tab 
gen agegr = autocode(alter,3,40,85)
oneway aktint13 agegr if inrange(d_rs,-10,10), tab 
drop aktint_h agegr


* aktanzs1 	Anzahl sozialer Aktivit�ten (mit jemanden zusammen)
* nur pc426 pc427 pc428 pc429 pc430 pc431 pc432
for any 26 28 29 30 31 32: gen aflagX =  inlist(pc4X,1,2,3)  & (inlist(pc4Xa_2,1)  ///
									     | inlist(pc4Xa_3,1) |  inlist(pc4Xa_4,1)  ///
										 | inlist(pc4Xa_5,1) |  inlist(pc4Xa_6,1)) 
										 
tab aflag30
egen aktanzs1 = anycount(aflag*), v(1)
tab2 aktanzs1 welle, m col
drop aflag*
oneway aktanzs1 welle if inrange(d_rs,-10,10), tab 

* aktanzs2	Anzahl sozialer Aktivit�ten (Art der Aktivit�t)
egen aktanzs2 = anycount(pc425_4 pc429 pc430 pc431 pc432), v(1 2 3)
oneway aktanzs2 welle if inrange(d_rs,-10,10), tab 

* aktdiv
gen 	aktdiv = aktint13 / aktanzg13
replace aktdiv = 0 if mi(aktdiv)
* histogram akdiv
oneway aktdiv welle if inrange(d_rs,-10,10), tab 



		
* Olis Aktivit�tsscore
* ---- Mittelwert aus anzahl aktivit�ten und der intensit�t derer
* ---- doppelt t-standardisiert
* t(mean(t(akanzg),t(akint)))
* letzte t-standadisierung erfolgt wie bei den anderen Variablen 

* Wellenspezifisch? Oli nachfragen
gen aktoli_h1 = .
gen aktoli_h2 = .

foreach num of numlist 1/4 {
qui sum aktanzg4 if welle == `num'
local sd `r(sd)'
local mean `r(mean)'
replace aktoli_h1 = ((aktanzg4 - `mean') / `sd' ) * 10 + 50 if welle == `num'

qui sum aktint if welle == `num'
local sd `r(sd)'
local mean `r(mean)'
replace aktoli_h2 = ((aktint - `mean') / `sd' ) * 10 + 50 if welle == `num'
}
egen aktoli1  = rowmean(aktoli_h1 aktoli_h2)
gen  aktoli2 = aktoli_h2/aktoli_h1
drop aktoli_*

oneway aktoli1 welle, tab 
oneway aktoli2 welle, tab

* Muss noch erg�nzt werden!
		
		
lab var aktanzg3	"Anzahl Aktivit�ten mind. w�chentl. (t�glich, mehrmal und einmal die Woche)"
lab var aktanzg4	"Anzahl Aktivit�ten mind. monatl. (t�glich, mehrmal und einmal die Woche)"
lab var aktanzg14	"Anzahl Aktivit�ten mind. monatl. 14 Akt(t�glich, mehrmal und einmal die Woche)"
lab var aktanzg13	"Anzahl Aktivit�ten mind. monatl. 13 Akt(t�glich, mehrmal und einmal die Woche)"
lab var aktint		"Intensit�t aller ausge�bten Aktivit�ten (Sumscore)"
lab var aktint14	"Intensit�t aller ausge�bten Aktivit�ten 14 Akt (Sumscore)"
lab var aktint13	"Intensit�t aller ausge�bten Aktivit�ten 13 Akt (Sumscore)"
lab var aktanzs1 	"Anzahl sozialer Aktivit�ten (mit jemanden zusammen, mind. w�chentl.)"
lab var aktanzs2	"Anzahl sozialer Aktivit�ten (Art der Aktivit�t, mind. w�chentl.)"
lab var aktdiv		"Diversit�t von Aktiviten (Intensit�t durch Anzahl)"
lab var aktoli1		"Anzahl und Intensit�t (t(mean(t(akanzg),t(akint)))"
lab var aktoli2		"Intensit�t pro Aktivit�t (t(t(akint)/t(akanzg))"

****************************************************************************************
***! Ehrenamt
****************************************************************************************

egen 	ehr1 = anymatch(pc4081 pc4082 pc4083 pc4084 pc4085), v(1)	// in Welle engagiert?
bys fallnum: egen ehr2 = total(ehr1)

tab ehr1 welle, col
lab var ehr1 "Ehrenamt: Aus�bung mind. einer Funktion"
tab ehr2 welle, col
tab ehramt welle, col


****************************************************************************************
***! Vorheriger Erwerbsstatus
****************************************************************************************
* Status vor Ruhestand pc106
tab pc106 welle, m
gen flag = !mi(rsj_kons)

bys flag: tab pc106 
drop flag

* Arbeitszeit (h/Woche) vor der Rente Xpc118
tab pc118 welle, m
tabstat pc118, s(n mean) by(pc106)
tab pc118 welle, m
replace pc118 = 80 if inrange(pc118,80,141)
replace pc118 =  0 if pc106 != 1
tab pc106, m

tab pc103c welle, m

 
****
* Lastjob
****
gen job = lastjob == 1
tab lastjob job , m
bys fallnum: gen n = _n
tab lastjob job if inrange(a_rs,57,65) & inrange(d_rs,-6,10) & n == 1 , m 
drop n 

bys fallnum: gen flag = 1 if lastjob != lastjob[_n+1] & fallnum == fallnum[_n+1]
bys fallnum: egen flag2 = total(flag)
tab flag2
drop flag flag2




